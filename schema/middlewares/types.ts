export type FieldType = {
    name: string,
    type: any,
    parentType: string,
    required?: boolean,
    authorized?: boolean,
}
