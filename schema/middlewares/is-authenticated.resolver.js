"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
const errors_1 = require("../errors");
const isAuthenticated = () => (next) => (root, args, context, info) => {
    if (context?.user) {
        return next(root, args, context, info);
    }
    if ((context.authorized === undefined || context.authorized === true) && !context.fields?.some((field) => !field.authorized)) {
        return next(root, args, context, info);
    }
    throw new graphql_1.GraphQLError(errors_1.NOT_AUTHORIZED);
};
exports.default = isAuthenticated;
