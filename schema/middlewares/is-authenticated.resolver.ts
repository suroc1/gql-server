import { GraphQLError } from "graphql";
import { GraphQLContext } from "../../types";
import { FieldType } from "./types";
import { NOT_AUTHORIZED } from "../errors";

const isAuthenticated = () => (next: any) => (root: any, args: any, context: GraphQLContext, info: any) => {
    if (context?.user) {
        return next(root, args, context, info);
    }

    if ((context.authorized === undefined || context.authorized === true) && !context.fields?.some((field: FieldType) => !field.authorized)) {
        return next(root, args, context, info);
    }
   
    throw new GraphQLError(NOT_AUTHORIZED);
}

export default isAuthenticated;
