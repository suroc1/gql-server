"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const selectionSet = () => (next) => (root, args, context, info) => {
    const selectionSet = info.fieldNodes[0].selectionSet?.selections.map((selection) => selection.name.value);
    if (!selectionSet) {
        return next(root, args, { ...context, fields: [] }, info);
    }
    const fields = context.fields?.filter((field) => 
    // filter out fields that are not in selection set
    selectionSet.includes(field.name));
    return next(root, args, { ...context, fields }, info);
};
const omitResolvers = (resolvers) => () => (next) => (root, args, context, info) => {
    const fields = context.fields?.filter((field) => 
    // filter out fields that have their own resolvers
    !Object.entries(resolvers).find(([parentType, typeResolvers]) => field.parentType === parentType && typeResolvers[field.name]));
    return next(root, args, { ...context, fields }, info);
};
exports.default = {
    selectionSet,
    omitResolvers,
};
