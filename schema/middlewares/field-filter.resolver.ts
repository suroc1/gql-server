import { GraphQLContext } from "../../types";
import { FieldType } from "./types";

const selectionSet = () => (next: any) => (root: any, args: any, context: GraphQLContext, info: any) => {
    const selectionSet = info.fieldNodes[0].selectionSet?.selections.map((selection: any) => selection.name.value);

    if (!selectionSet) {
        return next(root, args, { ...context, fields: [] }, info);
    }
    
    const fields = context.fields?.filter((field: FieldType) =>
        // filter out fields that are not in selection set
        selectionSet.includes(field.name)
    );

    return next(root, args, { ...context, fields }, info);
}

const omitResolvers = (resolvers: any) => () => (next: any) => (root: any, args: any, context: GraphQLContext, info: any) => {
    const fields = context.fields?.filter((field: FieldType) =>
        // filter out fields that have their own resolvers
        !Object.entries(resolvers).find(([parentType, typeResolvers]: [string, any]) =>
            field.parentType === parentType && typeResolvers[field.name]
        )
    );

    return next(root, args, { ...context, fields }, info);
};

export default {
    selectionSet,
    omitResolvers,
}
