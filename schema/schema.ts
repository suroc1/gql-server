import { makeExecutableSchema, mergeSchemas } from "@graphql-tools/schema";
import { GraphQLSchema } from "graphql";
import { composeResolvers } from "@graphql-tools/resolvers-composition";
import { auth as authDirective } from "./directives";
import fieldExtractorTransformer from "./transformers/field-extractor.transformer";
import fieldFilter from "./middlewares/field-filter.resolver";
import isAuthenticated from "./middlewares/is-authenticated.resolver";
import { getSchemas } from "./utils";

/**
 * Implementation based on guide: https://the-guild.dev/graphql/tools/docs/resolvers-composition
 */
export type ResolverMiddleware = () => (next: any) => (parent: any, args: any, context: any, info: any) => any;

export type SchemaTransformer = (schema: GraphQLSchema) => GraphQLSchema;

export type GraphQLAppliedDirectiveInitializer = (directiveName: string) => GraphQLAppliedDirective;

export type GraphQLAppliedDirective = {
    directiveName: string;
    typeDefs: string;
    transform: SchemaTransformer;
};

type Suroc1FieldsConfig = {
    // extract fields from schema and add to context
    extract: boolean;
    // filter extracted fields from context based on selection set
    filter?: Partial<{
        selectionSet: boolean;
        omitResolvers: boolean;
    }>;
};

type Suroc1SchemaOptions = {
    dir: string;
    // Typically contains of object containing resolvers for each type
    // Query, Mutation, Subscription + custom types
    resolvers: unknown;
    // Examples can be found at: https://the-guild.dev/graphql/tools/docs/schema-directives
    directives?: GraphQLAppliedDirective[];
    // Examples can be found at: https://the-guild.dev/graphql/tools/docs/resolvers-composition
    resolverMiddlewares?: ResolverMiddleware[];
    // Examples can be found at: https://the-guild.dev/graphql/tools/docs/schema-transformers
    schemaMiddlewares?: SchemaTransformer[];
    /** 
     * auth enables authentication middleware and @auth directive
     * Example:
     * type Helloworld {
     *    hello: String! @auth // gonna throw auth error if `user` field is not present in context
     *    world: String!
     * }
     *
     * @default false
    */
    auth?: boolean;
    /**
     * Enabled when `extract` is true or `auth` is true
     */
    fields?: Suroc1FieldsConfig;
};

class Suroc1Schema {
    private _schema: GraphQLSchema;

    private _directiveTransformers: SchemaTransformer[] = [];
    private _directiveTypeDefs: string[] = [];

    private _schemaMiddlewares: SchemaTransformer[] = [];

    private _resolverMiddlewares: ResolverMiddleware[] = [];

    constructor(options: Suroc1SchemaOptions) {
        const { resolvers, dir, directives, resolverMiddlewares, schemaMiddlewares, auth, fields } = options;

        if (!dir || dir == '' || !resolvers) throw new Error('dir and resolvers are required');

        const typeDefs = getSchemas(dir).map((file) => file.schema);

        this._resolverMiddlewares = resolverMiddlewares || [];

        if (auth) {
            this._directiveTypeDefs.push(authDirective.typeDefs);
            this._directiveTransformers.push(authDirective.transform);
            this._resolverMiddlewares.push(isAuthenticated);
        }

        directives?.forEach((directive) => {
            this._directiveTypeDefs.push(directive.typeDefs);
            this._directiveTransformers.push(directive.transform);
        });
        this._schemaMiddlewares = schemaMiddlewares || [];

        const rawSchema = mergeSchemas({
            typeDefs: [
                ...this._directiveTypeDefs,
                ...typeDefs
            ]
        });

        this._schema = makeExecutableSchema({
            typeDefs: rawSchema,
            resolvers: this.applyMiddlewaresToResolvers(resolvers),
        });

        if (fields?.extract || auth) {
            // apply fieldExtractorTransformer to schema
            this.applyMiddlewareToSchema(fieldExtractorTransformer(auth || false));
            // apply filters to extractedFields
            fields?.filter?.omitResolvers && this._resolverMiddlewares.push(fieldFilter.omitResolvers(resolvers));
            fields?.filter?.selectionSet && this._resolverMiddlewares.push(fieldFilter.selectionSet);
        }

        this.applyDirectivesToSchema();
        this.applyMiddlewaresToSchema();
    }

    get schema() {
        return this._schema;
    }

    private applyDirectivesToSchema() {
        if (!this._directiveTransformers.length) return;
        this._directiveTransformers.reduce((schema, directive) => directive(schema), this.schema);
    };

    private applyMiddlewaresToSchema () {
        if (!this._schemaMiddlewares.length) return;
        return this._schemaMiddlewares.reduce((schema, middleware) => middleware(schema), this._schema);
    };

    private applyMiddlewareToSchema (middleware: SchemaTransformer) {
        this._schema = middleware(this._schema);
    }

    
    /**
     * Implementation based on guide: https://the-guild.dev/graphql/tools/docs/resolvers-composition
     * @param resolvers 
     * @returns composedResolvers
     */
    private applyMiddlewaresToResolvers (resolvers: any) {
        if (!this._resolverMiddlewares.length) {
            return resolvers;
        }
        const compositionsMap = new Map<string, any[]>();
        Object.entries(resolvers).forEach(([parentType, typeResolvers]: [string, any]) => {
            Object.keys(typeResolvers).forEach((fieldName: string) => compositionsMap.set(
                `${parentType}.${fieldName}`,
                this._resolverMiddlewares.map((middleware) => middleware())
            ));
        });
    
        const compositions = Object.fromEntries(compositionsMap);
     
        const composedResolvers = composeResolvers(resolvers, compositions);
        return composedResolvers;
    };
}

export default (options: Suroc1SchemaOptions) => new Suroc1Schema(options).schema;
