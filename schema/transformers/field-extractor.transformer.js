"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const utils_1 = require("@graphql-tools/utils");
const graphql_1 = require("graphql");
const utils_2 = require("../utils");
const directives_1 = require("../directives");
exports.default = (authEnabled) => (schema) => {
    return (0, utils_1.mapSchema)(schema, {
        [utils_1.MapperKind.OBJECT_FIELD](fieldConfig) {
            const type = (0, graphql_1.getNamedType)(fieldConfig.type);
            if (type instanceof graphql_1.GraphQLObjectType || type instanceof graphql_1.GraphQLScalarType) {
                const { resolve = graphql_1.defaultFieldResolver } = fieldConfig;
                fieldConfig.resolve = function (source, args, context, info) {
                    let extendedContext = context;
                    if (!authEnabled) {
                        extendedContext = extractFields(context, info);
                    }
                    else {
                        extendedContext = extractFieldsWithAuth(fieldConfig, context, info);
                    }
                    return resolve(source, args, extendedContext, info);
                };
            }
            return fieldConfig;
        },
    });
};
const extractFields = (context, info) => {
    const type = (0, graphql_1.getNamedType)(info.returnType);
    if (!type)
        return context;
    const fields = type.astNode?.fields?.
        map((field) => getFieldType({
        field: field.type,
        parentType: type.name,
        name: field.name.value,
    }));
    return { ...context, fields };
};
const extractFieldsWithAuth = (fieldConfig, context, info) => {
    if (!context.authorized) {
        context = { ...context, authorized: !(0, utils_2.getDirectiveFromFieldConfig)(fieldConfig, directives_1.auth.directiveName) };
    }
    const type = (0, graphql_1.getNamedType)(info.returnType);
    if (!type)
        return context;
    const fields = type.astNode?.fields?.
        map((field) => getFieldType({
        field: field.type,
        parentType: type.name,
        name: field.name.value,
        authorized: !(0, utils_2.getDirectiveFromFieldDefinitionNode)(field, directives_1.auth.directiveName),
    }));
    return { ...context, fields };
};
const getFieldType = ({ field, name, required = false, authorized = true, parentType }) => {
    if (field.kind === graphql_1.Kind.NON_NULL_TYPE) {
        return getFieldType({ field: field.type, name, required: true, authorized, parentType });
    }
    else if (field.kind === graphql_1.Kind.LIST_TYPE) {
        return {
            name,
            parentType,
            type: `Array<${getFieldType({ field: field.type, name, parentType }).type}>`,
            required,
            authorized
        };
    }
    return {
        name,
        parentType,
        type: (field.kind === 'NamedType') && field.name.value,
        required,
        authorized
    };
};
