import { MapperKind, Maybe, mapSchema } from '@graphql-tools/utils';
import { GraphQLFieldConfig, GraphQLObjectType, GraphQLScalarType, GraphQLSchema, GraphQLType, Kind, TypeNode, defaultFieldResolver, getNamedType } from 'graphql';
import { getDirectiveFromFieldDefinitionNode, getDirectiveFromFieldConfig } from '../utils';
import { auth } from '../directives';
import { GraphQLContext } from '../../types';
import { FieldType } from '../middlewares/types';

type GetFieldTypeInput = {
    field: TypeNode,
    parentType: string,
    name: string,
    required?: boolean
    authorized?: boolean,
};

export default (authEnabled: boolean) => (schema: GraphQLSchema) => {
    return mapSchema(schema, {
        [MapperKind.OBJECT_FIELD](fieldConfig) {
            const type = getNamedType(fieldConfig.type);
            if (type instanceof GraphQLObjectType || type instanceof GraphQLScalarType) {
                const { resolve = defaultFieldResolver } = fieldConfig;
                fieldConfig.resolve = function (source, args, context: GraphQLContext, info) {
                    let extendedContext = context;
                    if (!authEnabled) {
                        extendedContext = extractFields(context, info);
                    } else {
                        extendedContext = extractFieldsWithAuth(fieldConfig, context, info);
                    }

                    return resolve(source, args, extendedContext, info);
                }
            }
            return fieldConfig;
        },
        
    })
};

const extractFields = (context: GraphQLContext, info: any) => {
        const type = getNamedType(info.returnType as Maybe<GraphQLType>);
        if (!type) return context;
        const fields = (type as GraphQLObjectType).astNode?.fields?.
        map((field) => getFieldType({
            field: field.type,
            parentType: type.name,
            name: field.name.value,
        }));

        return { ...context, fields };
    };

const extractFieldsWithAuth = (fieldConfig:  GraphQLFieldConfig<any, any, any>, context: GraphQLContext, info: any) => {
        if (!context.authorized) {
            context = { ...context, authorized: !getDirectiveFromFieldConfig(fieldConfig, auth.directiveName) };
        }

        const type = getNamedType(info.returnType as Maybe<GraphQLType>);
        if (!type) return context;

        const fields = (type as GraphQLObjectType).astNode?.fields?.
        map((field) => getFieldType({
            field: field.type,
            parentType: type.name,
            name: field.name.value,
            authorized: !getDirectiveFromFieldDefinitionNode(field, auth.directiveName),
        }));

        return { ...context, fields };
    };

const getFieldType = ({ field, name, required = false, authorized = true, parentType }: GetFieldTypeInput): FieldType => {
    if (field.kind === Kind.NON_NULL_TYPE) {
        return getFieldType({ field: field.type, name, required: true, authorized, parentType });
    } else if (field.kind === Kind.LIST_TYPE) {
        return {
            name,
            parentType,
            type: `Array<${getFieldType({ field: field.type, name, parentType }).type}>`,
            required,
            authorized
        };
    }

    return {
        name,
        parentType,
        type: (field.kind === 'NamedType') && field.name.value,
        required,
        authorized
    };
}
