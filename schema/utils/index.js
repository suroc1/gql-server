"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getSchemas = exports.getDirectiveFromFieldDefinitionNode = exports.getDirectiveFromFieldConfig = void 0;
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const getDirectiveFromFieldConfig = (node, directiveName) => {
    return node.astNode?.directives?.some((directive) => directive.name.value === directiveName);
};
exports.getDirectiveFromFieldConfig = getDirectiveFromFieldConfig;
const getDirectiveFromFieldDefinitionNode = (node, directiveName) => {
    return node.directives?.some((directive) => directive.name.value === directiveName);
};
exports.getDirectiveFromFieldDefinitionNode = getDirectiveFromFieldDefinitionNode;
const getSchemas = (dir) => {
    const dirPath = path_1.default.resolve(__dirname, dir);
    const files = fs_1.default.readdirSync(dirPath)
        .filter(file => file.endsWith('.gql') || file.endsWith('.graphql'))
        .map(file => ({
        name: file,
        schema: fs_1.default.readFileSync(path_1.default.join(dirPath, file), 'utf-8')
    }));
    return files;
};
exports.getSchemas = getSchemas;
