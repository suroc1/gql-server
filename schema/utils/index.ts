import { FieldDefinitionNode, GraphQLFieldConfig } from "graphql";
import fs from 'fs';
import path from 'path';

export type SchemaFile = {
    name: string;
    schema: string;
};

export const getDirectiveFromFieldConfig = (node: GraphQLFieldConfig<any, any, any>, directiveName: string) => {
    return node.astNode?.directives?.some((directive) => directive.name.value === directiveName);
};

export const getDirectiveFromFieldDefinitionNode = (node: FieldDefinitionNode, directiveName: string) => {
    return node.directives?.some((directive) => directive.name.value === directiveName);
};

export const getSchemas = (dir: string): SchemaFile[] => {
    const dirPath = path.resolve(__dirname, dir);

    const files = fs.readdirSync(dirPath)
    .filter(file => file.endsWith('.gql') || file.endsWith('.graphql'))
    .map(file => ({
        name: file,
        schema: fs.readFileSync(path.join(dirPath, file), 'utf-8')
    }));

    return files;
}
