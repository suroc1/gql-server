"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const schema_1 = require("@graphql-tools/schema");
const resolvers_composition_1 = require("@graphql-tools/resolvers-composition");
const directives_1 = require("./directives");
const field_extractor_transformer_1 = __importDefault(require("./transformers/field-extractor.transformer"));
const field_filter_resolver_1 = __importDefault(require("./middlewares/field-filter.resolver"));
const is_authenticated_resolver_1 = __importDefault(require("./middlewares/is-authenticated.resolver"));
const utils_1 = require("./utils");
class Suroc1Schema {
    _schema;
    _directiveTransformers = [];
    _directiveTypeDefs = [];
    _schemaMiddlewares = [];
    _resolverMiddlewares = [];
    constructor(options) {
        const { resolvers, dir, directives, resolverMiddlewares, schemaMiddlewares, auth, fields } = options;
        if (!dir || dir == '' || !resolvers)
            throw new Error('dir and resolvers are required');
        const typeDefs = (0, utils_1.getSchemas)(dir).map((file) => file.schema);
        this._resolverMiddlewares = resolverMiddlewares || [];
        if (auth) {
            this._directiveTypeDefs.push(directives_1.auth.typeDefs);
            this._directiveTransformers.push(directives_1.auth.transform);
            this._resolverMiddlewares.push(is_authenticated_resolver_1.default);
        }
        directives?.forEach((directive) => {
            this._directiveTypeDefs.push(directive.typeDefs);
            this._directiveTransformers.push(directive.transform);
        });
        this._schemaMiddlewares = schemaMiddlewares || [];
        const rawSchema = (0, schema_1.mergeSchemas)({
            typeDefs: [
                ...this._directiveTypeDefs,
                ...typeDefs
            ]
        });
        this._schema = (0, schema_1.makeExecutableSchema)({
            typeDefs: rawSchema,
            resolvers: this.applyMiddlewaresToResolvers(resolvers),
        });
        if (fields?.extract || auth) {
            // apply fieldExtractorTransformer to schema
            this.applyMiddlewareToSchema((0, field_extractor_transformer_1.default)(auth || false));
            // apply filters to extractedFields
            fields?.filter?.omitResolvers && this._resolverMiddlewares.push(field_filter_resolver_1.default.omitResolvers(resolvers));
            fields?.filter?.selectionSet && this._resolverMiddlewares.push(field_filter_resolver_1.default.selectionSet);
        }
        this.applyDirectivesToSchema();
        this.applyMiddlewaresToSchema();
    }
    get schema() {
        return this._schema;
    }
    applyDirectivesToSchema() {
        if (!this._directiveTransformers.length)
            return;
        this._directiveTransformers.reduce((schema, directive) => directive(schema), this.schema);
    }
    ;
    applyMiddlewaresToSchema() {
        if (!this._schemaMiddlewares.length)
            return;
        return this._schemaMiddlewares.reduce((schema, middleware) => middleware(schema), this._schema);
    }
    ;
    applyMiddlewareToSchema(middleware) {
        this._schema = middleware(this._schema);
    }
    /**
     * Implementation based on guide: https://the-guild.dev/graphql/tools/docs/resolvers-composition
     * @param resolvers
     * @returns composedResolvers
     */
    applyMiddlewaresToResolvers(resolvers) {
        if (!this._resolverMiddlewares.length) {
            return resolvers;
        }
        const compositionsMap = new Map();
        Object.entries(resolvers).forEach(([parentType, typeResolvers]) => {
            Object.keys(typeResolvers).forEach((fieldName) => compositionsMap.set(`${parentType}.${fieldName}`, this._resolverMiddlewares.map((middleware) => middleware())));
        });
        const compositions = Object.fromEntries(compositionsMap);
        const composedResolvers = (0, resolvers_composition_1.composeResolvers)(resolvers, compositions);
        return composedResolvers;
    }
    ;
}
exports.default = (options) => new Suroc1Schema(options).schema;
