import { MapperKind, mapSchema } from '@graphql-tools/utils';
import { GraphQLSchema, defaultFieldResolver } from 'graphql';
import { getDirectiveFromFieldConfig } from '../utils';
import { GraphQLContext } from '../../types';
import { GraphQLAppliedDirective, GraphQLAppliedDirectiveInitializer } from '../schema';

export const auth: GraphQLAppliedDirectiveInitializer = (directiveName: string): GraphQLAppliedDirective => {
    return {
        directiveName,
        typeDefs: `directive @${directiveName} on FIELD_DEFINITION`,
        transform: (schema: GraphQLSchema) =>
            mapSchema(schema, {
            [MapperKind.OBJECT_FIELD](fieldConfig) {
                const authDirective = getDirectiveFromFieldConfig(fieldConfig, directiveName);
                if (authDirective) {
                    const { resolve = defaultFieldResolver } = fieldConfig;
                    fieldConfig.resolve = function (source, args, context: GraphQLContext, info) {
                        if (!context?.user) {
                            context = { ...context, authorized: false };
                        }
                        return resolve(source, args, context, info);
                    }
                }

                return fieldConfig;
            },
        })
    }
}
