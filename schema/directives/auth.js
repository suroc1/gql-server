"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.auth = void 0;
const utils_1 = require("@graphql-tools/utils");
const graphql_1 = require("graphql");
const utils_2 = require("../utils");
const auth = (directiveName) => {
    return {
        directiveName,
        typeDefs: `directive @${directiveName} on FIELD_DEFINITION`,
        transform: (schema) => (0, utils_1.mapSchema)(schema, {
            [utils_1.MapperKind.OBJECT_FIELD](fieldConfig) {
                const authDirective = (0, utils_2.getDirectiveFromFieldConfig)(fieldConfig, directiveName);
                if (authDirective) {
                    const { resolve = graphql_1.defaultFieldResolver } = fieldConfig;
                    fieldConfig.resolve = function (source, args, context, info) {
                        if (!context?.user) {
                            context = { ...context, authorized: false };
                        }
                        return resolve(source, args, context, info);
                    };
                }
                return fieldConfig;
            },
        })
    };
};
exports.auth = auth;
