"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fastify_1 = __importDefault(require("fastify"));
const cors_1 = __importDefault(require("@fastify/cors"));
const formbody_1 = __importDefault(require("@fastify/formbody"));
const fastify_2 = require("graphql-http/lib/use/fastify");
const node_path_1 = __importDefault(require("node:path"));
const ejs_1 = __importDefault(require("ejs"));
class Suroc1FastifyServer {
    app;
    config;
    handlerOptions;
    constructor(options) {
        this.app = (0, fastify_1.default)(options);
        this.config = options;
        this.handlerOptions = options;
    }
    init() {
        this.app.register(cors_1.default);
        this.app.register(formbody_1.default);
        const { prefix, graphiql, middlewaresOnRequest, middlewaresPreHandler } = this.config;
        const { schema, context } = this.handlerOptions;
        this.app.register(async function (api) {
            const bodySchema = {
                type: "object",
                properties: {
                    query: { type: "string" },
                    operationName: { type: "string" },
                    variables: { type: "object" },
                    extensions: { type: "object" },
                },
                required: ["query"],
            };
            if (graphiql) {
                // Graphiql
                const graphiqlAppUrl = node_path_1.default.join(__dirname, "../schema/graphiql");
                api.get("/", async (request, reply) => {
                    const graphiqlFileToRender = node_path_1.default.join(graphiqlAppUrl, "index.ejs");
                    const staticToSend = await ejs_1.default.renderFile(graphiqlFileToRender, {
                        url: prefix
                    });
                    return reply.type('text/html').send(staticToSend);
                });
            }
            else {
                api.get("/", () => {
                    return { hello: "suroc1" };
                });
            }
            // Graphql API
            api.post("/", {
                // onRequest doesn't parse body
                onRequest: [...(middlewaresOnRequest ?? [])],
                // preHandler parses body
                preHandler: [...(middlewaresPreHandler ?? [])],
                // schema validates body
                schema: { body: bodySchema }
            }, (0, fastify_2.createHandler)({
                schema,
                context,
            }));
        }, { prefix });
        return this.app;
    }
}
exports.default = (options) => new Suroc1FastifyServer(options).init();
