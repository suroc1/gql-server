import fastify, { FastifyBaseLogger, FastifyHttpOptions, FastifyInstance } from "fastify";
import cors from "@fastify/cors";
import fastifyFormbody from "@fastify/formbody";
import { createHandler, HandlerOptions } from "graphql-http/lib/use/fastify";
import path from "node:path";
import ejs from "ejs";
import * as http from "http";
import { FasstifyMiddleware, FastifyAsyncMiddleware, GraphQLContext } from "../types.d";
import { GraphQLSchema } from "graphql";

type Suroc1ServerOptions = Suroc1ServerConfig & FastifyHttpOptions<http.Server, FastifyBaseLogger> & Suroc1HandlerOptions;

interface Suroc1HandlerOptions extends HandlerOptions<GraphQLContext> {
    schema: GraphQLSchema;
}

type Suroc1ServerConfig = {
    graphiql?: boolean | false;
    // default is /graphql
    prefix?: string;
    middlewaresOnRequest?: FasstifyMiddleware[];
    middlewaresPreHandler?: FastifyAsyncMiddleware[];
};

class Suroc1FastifyServer {
    private app: FastifyInstance;
    private config: Suroc1ServerConfig;
    private handlerOptions: Suroc1HandlerOptions;
    constructor(options: Suroc1ServerOptions) {
        this.app = fastify(options);
        this.config = options;
        this.handlerOptions = options;
    }

    init(): FastifyInstance {
        this.app.register(cors);
        this.app.register(fastifyFormbody);
        const { prefix, graphiql, middlewaresOnRequest, middlewaresPreHandler } = this.config;
        const { schema, context } = this.handlerOptions;
        this.app.register(async function (api) {
            const bodySchema = {
                type: "object",
                properties: {
                    query: { type: "string" },
                    operationName: { type: "string" },
                    variables: { type: "object" },
                    extensions: { type: "object" },
                },
                required: ["query"],
            };

            if (graphiql) {
                // Graphiql
                const graphiqlAppUrl = path.join(__dirname, "../schema/graphiql");
                api.get("/", async (request, reply) => {
                    const graphiqlFileToRender = path.join(graphiqlAppUrl, "index.ejs");

                    const staticToSend = await ejs.renderFile(graphiqlFileToRender, {
                        url: prefix
                    });

                    return reply.type('text/html').send(staticToSend);
                });
            } else {
                api.get("/", () => {
                    return { hello: "suroc1" };
                });
            }

            // Graphql API
            api.post("/", {
                // onRequest doesn't parse body
                onRequest: [...(middlewaresOnRequest ?? [])],
                // preHandler parses body
                preHandler: [...(middlewaresPreHandler ?? [])],
                // schema validates body
                schema: { body: bodySchema }
            }, createHandler({
                schema,
                context,
            }));
        }, { prefix });
        return this.app;
    }
}

export default (options: Suroc1ServerOptions) => new Suroc1FastifyServer(options).init();
