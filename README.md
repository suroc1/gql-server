Package is a wrap around `fastify`, `graphql-http` and various `@graphql-tools` to simplify development of graphql applications

## Instalation
`npm i @suroc1/graphql`

## How to use
```
import surocShema from "@suroc1/graphql/schema";
import surocServer from "@suroc1/graphql/server";

const resolvers = {
    Mutation: {
        hello: () => {}
    },
    Query: {
        world: () => {}
    },
    YourType: {
        resolver: () => {}
    }
};

const schema = surocShema({
    dir: '/path/to/your/gql/files/folder',
    resolvers,
})

const app = surocServer({
    schema,
});

app.listen({ host: config.appHost, port: parseInt(config.appPort) }, async (err, address) => {
    if (err) {
        app.log.error(err);
        process.exit(1);
    }
    console.log(`🚀 Server ready at: ${address}`);
});
```

## Server Options
```
// combination of two types bellow and fastifyHttpOptions
type Suroc1ServerOptions = Suroc1ServerConfig & FastifyHttpOptions<http.Server, FastifyBaseLogger> & Suroc1HandlerOptions;

interface Suroc1HandlerOptions extends HandlerOptions<GraphQLContext> {
    schema: GraphQLSchema; // schema is required to run the server
}

type Suroc1ServerConfig = {
    graphiql?: boolean | false; // enables visual representation of schema using https://www.npmjs.com/package/graphiql
    prefix?: string; // default is /graphql
    middlewaresOnRequest?: []; // fastify OnRequest function, order of middlewares matters,
    middlewaresPreHandler?: []; // fastify Prehandler function, order of middlewares matters
};
```

## Schema options
```
type Suroc1SchemaOptions = {
    dir: string; // location of .gql files

    // Typically contains of object containing resolvers for each type
    resolvers: unknown; // Query, Mutation, Subscription + custom types
    
    directives?: GraphQLAppliedDirective[]; // Examples can be found at: https://the-guild.dev/graphql/tools/docs/schema-directives
    
    resolverMiddlewares?: ResolverMiddleware[]; // Examples can be found at: https://the-guild.dev/graphql/tools/docs/resolvers-composition
    
    schemaMiddlewares?: SchemaTransformer[]; // Examples can be found at: https://the-guild.dev/graphql/tools/docs/schema-transformers
    /** 
     * auth enables authentication middleware and @auth directive
     * Example:
     * type Helloworld {
     *    hello: String! @auth // gonna throw auth error if `user` field is not present in context
     *    world: String!
     * }
     *
     * @default false
    */
    auth?: boolean;
    /**
     * Enabled when `extract` is true or `auth` is true
     */
    fields?: Suroc1FieldsConfig; // fields for specific resolver
};
```
