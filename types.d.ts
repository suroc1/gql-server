import { DoneFuncWithErrOrRes, FastifyReply, FastifyRequest as OriginalFastifyRequest } from 'fastify';
import { DocumentNode, Source, Dire, GraphQLSchema } from 'graphql';

export type FasstifyMiddleware = (request: OriginalFastifyRequest, reply: FastifyReply, done: DoneFuncWithErrOrRes) => void;
export type FastifyAsyncMiddleware = (request: OriginalFastifyRequest, reply: FastifyReply) => Promise<void>;

export type GraphQLContext = {
    user?: any;
    fields?: FieldType[],
    authorized?: boolean
};

declare module 'fastify' {
    export interface FastifyRequest extends OriginalFastifyRequest {
        user: Authorisation;
    }
}
